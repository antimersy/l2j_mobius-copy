L2J-Mobius Underground

Client: https://drive.google.com/uc?id=0Bz1qNvCniabUMDhOd3RpUzBNSW8&export=download
Mirror: https://mega.nz/#!h1FXyByZ!XmyZBtk4qSOlKb435rx7X372_NreN1PTH8l7A4lNCoo
System: https://mega.nz/#!Rx1SCIBC!W5pM7xljB_6UwXaHp_aW8sqY1SJXp5LFa121YGRrb74

JDK: http://www.mediafire.com/file/cgh3zupv80qdwv4/bellsoft-jdk15.0.2%252B10-windows-amd64.msi
Eclipse: http://www.mediafire.com/file/h0gmazpv9hm6gjp/eclipse-java-2020-12-R-win32-x86_64.zip
Geodata: http://www.mediafire.com/file/jme7lm3imhu97m5/L2J_Mobius_2.5_Underground_Geodata.zip


Working features...

Before Goddess of Destruction:
-Fantasy Isle Parade
-Pailaka Injured Dragon
-All Seven Sign Quests
-Imperial Tomb
-Gracia Area
-Kamaloka
-Castle Dungeons
-Fortress Dungeons
-Prime Shop

Goddess of Destruction:
-New Talking Island
-Ancient City Arcan
-Bloody Swampland
-Fairy Settlement
-Garden of Genesis
-Guillotine Fortress
-Orbis Temple
-Kartia
-Fortuna
-Nursery
-Altar of Shilen
-Kimerian
-Istina
-Octavis
-Spezion
-Tauti
-Teredor
-Trasken
-Lindvior
-Appearance stones
-Beauty Shop
-Mentoring
-Goddess class changes
-Harnak Underground Ruins
-Sayune Jump System
-Agent of Chaos
-Attribute Crystals
-Ability Points
-Training Camp

Ertheia:
-Ertheia Race
-Alchemy
-Brooch system
-Luck system
-Auto fishing
-Dimensional Warp

Infinite Odyssey:
-Garden of Spirits
-Isabella
-Atelia Fortress
-Kelbim
-Enchanted Valley
-Ashen Shadow Revolutionaries

Underground:
-Underground Gainak
-Automated soulshots
-Daily rewards
-Attendance rewards

Events:
-Birth of Draco
-Character Birthday
-Eve the Fortune Teller
-Freya Celebration
-Gift of Vitality
-Halloween
-Heavy Medal
-Hungry Horse
-Hunt for Santa
-Letter Collector
-Lovers Jubilee
-Love your Gatekeeper
-Master of Enchanting
-The Power of Love
-Custom Elpies Event
-Custom Rabbits Event
-Custom Race Event
-Custom Team vs Team

Customs:
-Allowed player races
-Auto potions command
-Banking
-Champion monsters
-Classmaster
-Community board
-Faction system
-Fake players
-Find PvP
-NPC stat multipliers
-Realtime offline trade
-Password change
-Pc Cafe
-Premium system
-Private store range
-PvP announce
-PvP reward item
-PvP title color
-Random spawns
-Sayune for all
-Screen welcome message
-Sell buffs
-Starting location
-Vote reward
-Automated database backups

TODO:
-Mystic Tavern instanced zones
